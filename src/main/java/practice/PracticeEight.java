package practice;

import java.io.IOException;
import java.util.List;


public class PracticeEight {

    public static void main(String[] args) {
        try {
            PracticeEight ce = new PracticeEight();
            ce.getGamesInfo("Batman");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<SteamGame> getGamesInfo(String keywords) throws IOException {
        return null;
    }


    public class SteamGame {
        private String name;

        public String getLanzamiento() {
            return lanzamiento;
        }

        public void setLanzamiento(String lanzamiento) {
            this.lanzamiento = lanzamiento;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        private String lanzamiento;
        private String description;
        private String image;
        private String price;


    }
}
