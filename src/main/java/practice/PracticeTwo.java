package practice;


import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * Ejercicios practica 2
 */
public class PracticeTwo {

    public static String URL_TO_PARSE = "http://www.marca.com/";

    public static void main(String[] args) throws IOException {
        PracticeTwo.getLigaResultsUsingDOMNavigation();
    }

    /**
     *  TODO: Obtener los resultados de la jornada de marca.com usando class names
     */
    public static void getLigaResultsUsingClassNames() throws IOException {

    }

    /**
     *  TODO: Obtener los resultados de la jornada de marca.com usando navegación DOM
     */
    public static void getLigaResultsUsingDOMNavigation() throws IOException {

    }

}
