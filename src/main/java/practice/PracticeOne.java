package practice;


import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;

/**
 * Ejercicios practica 1
 */
public class PracticeOne {

    public static String BODY_FRAGMENT =  "<h1>Hola Mundo</h1>";

    public static String HTML_AS_STRING = "<html>" +
                                                    "<head>" +
                                                        "<title>Titulo de la practica 1</title>" +
                                                    "</head>" +
                                                    "<body>" +
                                                          BODY_FRAGMENT +
                                                    "</body>" +
                                                   "</html>";

    public static String URL_TO_PARSE = "http://jsoup.org";

    /**
     *  TODO: 1.1: Generar el Document a partir del string HTML_AS_STRING
     */
    public static Document parseHtmlFromString() {
        return null;
    }

    /**
     *  TODO: 1.2: Generar el Document a partir del archivo practiceOne.html
     */
    public static Document parseHtmlFromFile() throws IOException {
        return null;
    }

    /**
     *  TODO: 1.3: Generar el Document a partir del fragmento de body BODY_FRAGMENT
     */
    public static Document parseHtmlFromBodyFragment() {
        return null;
    }

    /**
     *  TODO: 1.4: Generar el Document a partir de la url URL_TO_PARSE
     */
    public static Document parseHtmlFromUrl() throws IOException {
        return null;
    }

}
