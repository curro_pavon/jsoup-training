package practice;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Iterator;

/**
 * Ejercicios practica 4
 */
public class PracticeFour {

    public static String INITIAL_HTML = "<html>" +
                                                "<head>" +
                                                    "<title>Practica 4</title>" +
                                                "</head>" +
                                                "<body>" +
                                                "</body>" +
                                            "</html>";

    public static void main(String[] args) throws IOException {
        PracticeFour.modifyHtml();
    }

    /**
     *  TODO 4.1: Modifica el HTML para crear una estructura DOM que contenga:
     *      - Un div con class container en el que ira el cuerpo del body
     *      - En el dic container crearemos una tabla de dimensiones 2 columnas x 5 filas y con border = 1
     *      - La tabla contendrá los datos Nombre, Apellidos, DNI, Telefono, Años experiencia.
     */
    public static String modifyHtml() throws IOException {
        return null;
    }
}
