package practice;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Iterator;

/**
 * Ejercicios practica 3
 */
public class PracticeThree {

    public static String URL_TO_PARSE = "http://www.segundamano.es/motor-de-segunda-mano/";

    public static void main(String[] args) throws IOException {
        PracticeThree.getSegundaManoResultsUsingCssSelectors1();
    }

    /**
     *  TODO 3.1: Imprimir por pantalla los datos de cada articulo (precio, descripcion y fecha)
     */
    public static void getSegundaManoResultsUsingCssSelectors1() throws IOException {

    }

    /**
     *  TODO 3.2: Imprimir por pantalla los datos de cada articulo (precio, descripcion y fecha)
     */
    public static void getSegundaManoResultsUsingCssSelectors2() throws IOException {

    }

    /**
     *  TODO 3.1: Imprimir por pantalla los datos de cada articulo (precio, descripcion y fecha)
     */
    public static void getSegundaManoResultsUsingCssSelectors3() throws IOException {

    }
}
